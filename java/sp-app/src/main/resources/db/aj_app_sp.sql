/*
 Navicat Premium Data Transfer

 Source Server         : 10.108.26.197gaea-dev
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 10.108.26.197:3306
 Source Schema         : aj_app_sp

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 09/10/2021 16:08:06
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for push_configures
-- ----------------------------
DROP TABLE IF EXISTS `push_configures`;
CREATE TABLE `push_configures`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `app_id`           bigint(20) NOT NULL COMMENT '应用ID',
    `app_key`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `secret_key`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务加密使用',
    `jg_app_key`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '极光appkey',
    `jg_master_secret` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '极光masterSecret',
    `hw_app_id`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '华为appId',
    `hw_app_secret`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '华为AppSecret',
    `hw_default_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '华为推送标题（默认建议应用名）',
    `op_app_id`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oppo appId',
    `op_app_secret`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oppo AppSecret',
    `op_default_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oppo 推送标题（默认建议应用名）',
    `op_app_key`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oppo AppKey',
    `op_master_secret` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'oppo Master Secret',
    `vo_app_id`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'vivo appId',
    `vo_app_secret`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'vivo AppSecret',
    `vo_default_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'vivo 推送标题（默认建议应用名）',
    `vo_app_key`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'vivo AppKey',
    `xm_app_id`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小米appId',
    `xm_app_secret`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小米AppSecret',
    `xm_default_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小米推送标题（默认建议应用名）',
    `xm_app_key`       varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '华为AppKey',
    `package_name`     varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '包名',
    `enable_flag`      int(11) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
    `delete_flag`      int(11) NOT NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
    `create_by`        bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`      datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`        bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`      datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`          varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    `channel_enable`   int(11) NOT NULL DEFAULT 0 COMMENT '是否开启厂商通道 默认 0',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推送配置项' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for push_history
-- ----------------------------
DROP TABLE IF EXISTS `push_history`;
CREATE TABLE `push_history`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `app_key`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `msg_id`           varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息唯一id',
    `device_ids`       text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '设备唯一标识',
    `registration_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '极光唯一标识',
    `target_type`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1:华为，2:小米，3:oppo，4:vivo，5：极光iOS 6：极光Android ',
    `target_num`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '目标（对应当次发送的条数）',
    `success_num`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '成功条数（对应当次发送的条数）',
    `title`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
    `content`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
    `extras`           text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '传递数据',
    `ios_config`       text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'iOS配置项 json',
    `android_config`   text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'android配置项 json {\"sound\": \"sound\"}',
    `send_origin`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '消息来源 0 web 1 api',
    `push_type`        varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送类型 1透传消息 0 普通消息',
    `channel`          varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道类型 华为厂商还是其他厂商',
    `push_result`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接收结果是否成功 1成功 0不成功',
    `send_time`        datetime(0) NOT NULL COMMENT '发送时间',
    `enable_flag`      int(11) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
    `delete_flag`      int(11) NOT NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
    `create_by`        bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`      datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`        bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`      datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`          text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4282 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推送历史表' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for push_message
-- ----------------------------
DROP TABLE IF EXISTS `push_message`;
CREATE TABLE `push_message`
(
    `id`                bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `app_key`           varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `msg_id`            varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息唯一id',
    `oper_param`        text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息内容消息json',
    `target_num`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '目标（对应消息推送总数）',
    `success_num`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '成功条数（对应消息推送成功总数）',
    `consumption_state` int(11) NOT NULL DEFAULT 0 COMMENT '消费状态 默认未消费 0： 未消费 1：已消费',
    `enable_flag`       int(11) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
    `delete_flag`       int(11) NOT NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
    `create_by`         bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`       datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`         bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`       datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`           text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注信息',
    `send_origin`       varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '消息来源 0 web 1 api',
    `push_type`         varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送类型 1透传消息 0 普通消息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1335 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务推送id记录' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for push_user
-- ----------------------------
DROP TABLE IF EXISTS `push_user`;
CREATE TABLE `push_user`
(
    `id`              bigint(20) NOT NULL AUTO_INCREMENT,
    `app_key`         varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `device_id`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备唯一标识',
    `manu_token`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '厂商通道的token或者regId',
    `device_type`     varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备类型：0:其他手机，1:华为，2:小米，3:oppo，4:vivo，5：ios',
    `alias`           varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '别名',
    `registration_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '极光的用户id',
    `brand`           varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备品牌（例如：小米6，iPhone 7plus）',
    `os_version`      varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统版本例如：7.1.2  13.4.1',
    `enable_flag`     int(11) NOT NULL DEFAULT 1 COMMENT '0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG',
    `delete_flag`     int(11) NOT NULL DEFAULT 0 COMMENT ' 0--未删除 1--已删除 DIC_NAME=DEL_FLAG',
    `create_by`       bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`     datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`       bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`     datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`         varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for sp_app_device
-- ----------------------------
DROP TABLE IF EXISTS `sp_app_device`;
CREATE TABLE `sp_app_device`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `app_key`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `device_id`   varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备唯一标识',
    `platform`    varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平台(iOS/Android)',
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 244 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'app设备唯一标识表' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for sp_app_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_app_log`;
CREATE TABLE `sp_app_log`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `app_key`             varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `device_id`           varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备唯一标识',
    `ip`                  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
    `regional`            varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地域（中国|0|上海|上海市|电信）（0|0|0|内网IP|内网IP）',
    `net_work_status`     varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联网方式',
    `platform`            varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平台(iOS/Android)',
    `screen_info`         varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '高*宽',
    `brand`               varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备品牌（例如：小米6，iPhone 7plus）',
    `version_name`        varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本名',
    `version_code`        varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
    `sdk_version`         varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sdk版本',
    `os_version`          varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统版本例如：7.1.2  13.4.1',
    `interface_type`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口类型',
    `interface_type_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口类型名',
    `enable_flag`         int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag`         int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`           bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`         datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`           bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`         datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`             varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'app请求log' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_app_log
-- ----------------------------

-- ----------------------------
-- Table structure for sp_app_release
-- ----------------------------
DROP TABLE IF EXISTS `sp_app_release`;
CREATE TABLE `sp_app_release`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT,
    `app_key`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `device_id`    varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备唯一标识',
    `version_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本名',
    `enable_flag`  int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag`  int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`    bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`  datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`    bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`  datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`      varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'app灰度发布用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_app_release
-- ----------------------------

-- ----------------------------
-- Table structure for sp_application
-- ----------------------------
DROP TABLE IF EXISTS `sp_application`;
CREATE TABLE `sp_application`
(
    `app_id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `app_key`              varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用唯一key',
    `name`                 varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用名称',
    `public_key`           varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公钥',
    `private_key`          varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '私钥',
    `enable_flag`          int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag`          int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`            bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`          datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`            bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`          datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`              varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    `logo_url`             varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'logo Url',
    `promote_version`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推广版本/环境信息',
    `promote_name`         varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推广应用名',
    `promote_desc`         varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推广语',
    `promote_introduction` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推广介绍',
    PRIMARY KEY (`app_id`) USING BTREE,
    UNIQUE INDEX `app_key_unique`(`app_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用' ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for sp_dict
-- ----------------------------
DROP TABLE IF EXISTS `sp_dict`;
CREATE TABLE `sp_dict`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `name`        varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '标签名',
    `value`       varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '数据值',
    `type`        varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '类型（Android版本、iOS版本、公告类型、展示类型）',
    `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述（Android版本、iOS版本、公告类型、展示类型）',
    `parent_id`   bigint(2) NULL DEFAULT 0 COMMENT '父级编号：0代表没有父级',
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX         `sys_dict_value`(`value`) USING BTREE,
    INDEX         `sys_dict_label`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_dict
-- ----------------------------
INSERT INTO `sp_dict`
VALUES (1, '6', 'Android6', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (2, '7', 'Android7', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (3, '8', 'Android8', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (4, '9', 'Android9', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (5, '10', 'Android10', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (6, '10', 'iOS10', 'IOS_VERSION', 'iOS版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-26 18:48:53', NULL);
INSERT INTO `sp_dict`
VALUES (7, '11', 'iOS11', 'IOS_VERSION', 'iOS版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-26 18:40:44', NULL);
INSERT INTO `sp_dict`
VALUES (8, '12', 'iOS12', 'IOS_VERSION', 'iOS版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (9, '13', 'iOS13', 'IOS_VERSION', 'iOS版本号', 0, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (10, '14', 'iOS14', 'IOS_VERSION', 'iOS版本号', 0, 1, 0, 2, '2020-10-23 14:08:09', 2, '2020-10-23 14:08:09', NULL);
INSERT INTO `sp_dict`
VALUES (11, 'horizontal_scroll', '水平滚动', 'TEMPLATE', '模板类型', 0, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (12, 'dialog', '弹框', 'TEMPLATE', '模板类型', 0, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_dict`
VALUES (13, '11', 'Android11', 'ANDROID_VERSION', 'Android版本号', 0, 1, 0, 3, '2021-07-16 17:08:00', 3,
        '2021-07-16 17:08:00', NULL);

-- ----------------------------
-- Table structure for sp_file
-- ----------------------------
DROP TABLE IF EXISTS `sp_file`;
CREATE TABLE `sp_file`
(
    `file_id`     varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '文件性一id',
    `file_path`   varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件在linux中的完整目录，比如/app/file-sp/file/${fileid}.xlsx',
    `url_path`    varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通过接口的下载完整http路径',
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for sp_menu
-- ----------------------------
DROP TABLE IF EXISTS `sp_menu`;
CREATE TABLE `sp_menu`
(
    `menu_id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `parent_id`   bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
    `name`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
    `url`         varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
    `perms`       varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
    `type`        int(10) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
    `icon`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
    `order_num`   int(2) NULL DEFAULT NULL COMMENT '排序',
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_menu
-- ----------------------------
INSERT INTO `sp_menu`
VALUES (1, 0, '账号管理', NULL, NULL, 0, NULL, 10, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_menu`
VALUES (2, 1, '版本管理', NULL, 'system:user:version', 1, NULL, 100, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_menu`
VALUES (3, 1, '公告管理', NULL, 'system:user:notice', 1, NULL, 101, 1, 0, 1, '2020-06-19 12:24:50', 1,
        '2020-06-19 12:24:56', NULL);
INSERT INTO `sp_menu`
VALUES (4, 1, '推送管理', NULL, 'system:user:push', 1, NULL, 102, 1, 0, 1, '2020-06-19 12:24:50', 1, '2020-06-19 12:24:56',
        NULL);
INSERT INTO `sp_menu`
VALUES (5, 0, '基础设置', NULL, NULL, 0, NULL, 11, 1, 0, 1, '2020-07-01 12:21:58', 1, '2020-07-01 12:22:04', NULL);
INSERT INTO `sp_menu`
VALUES (6, 1, '成员管理', NULL, 'system:user:members', 1, NULL, 103, 1, 0, 1, '2021-01-07 16:54:41', 1,
        '2021-01-07 16:54:45', NULL);

-- ----------------------------
-- Table structure for sp_notice
-- ----------------------------
DROP TABLE IF EXISTS `sp_notice`;
CREATE TABLE `sp_notice`
(
    `id`                 bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `app_id`             bigint(20) NOT NULL COMMENT '应用ID',
    `name`               varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '公告名称',
    `title`              varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '公告标题',
    `details`            varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告内容',
    `template_type`      varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '模板类型（来自字典表）',
    `template_type_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '模板类型名',
    `result_type`        int(10) NULL DEFAULT NULL COMMENT '效果类型（来自字典表）',
    `result_type_name`   varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '效果类型名',
    `start_time`         datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
    `end_time`           datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
    `enable_flag`        int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag`        int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`          bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`        datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`          bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`        datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`            varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '公告管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_notice
-- ----------------------------
INSERT INTO `sp_notice`
VALUES (1, 3, '测试公告', '公告标题', '公告内容', 'dialog', '弹框', NULL, NULL, '2021-03-02 12:22:12', '2021-03-31 00:00:00', 0, 0, 3,
        '2020-12-15 11:24:06', 3, '2021-03-01 17:53:41', NULL);

-- ----------------------------
-- Table structure for sp_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_oper_log`;
CREATE TABLE `sp_oper_log`
(
    `oper_id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
    `title`          varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
    `business_type`  int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
    `method`         varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
    `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
    `oper_name`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
    `app_id`         bigint(20) NULL DEFAULT NULL COMMENT '应用id',
    `oper_url`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
    `oper_ip`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
    `oper_location`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
    `oper_param`     text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '请求参数',
    `json_result`    text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回参数',
    `status`         int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
    `error_msg`      varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
    `oper_time`      datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
    `begin_time`     bigint(20) NULL DEFAULT NULL COMMENT '请求开始时间戳',
    `end_time`       bigint(20) NULL DEFAULT NULL COMMENT '请求结束时间戳',
    `time`           bigint(20) NULL DEFAULT NULL COMMENT '接口耗时-毫秒',
    PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 296 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for sp_role
-- ----------------------------
DROP TABLE IF EXISTS `sp_role`;
CREATE TABLE `sp_role`
(
    `role_id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `role_name`   varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
    `role_sign`   varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色标识',
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_role
-- ----------------------------
INSERT INTO `sp_role`
VALUES (1, '产品经理', '1', 1, 0, 1, '2020-06-19 12:11:59', 2, '2021-03-17 18:12:16', '产品角色');
INSERT INTO `sp_role`
VALUES (2, '测试人员', '1', 1, 0, 1, '2020-06-19 12:11:59', 3, '2021-09-13 14:50:27', '测试角色');
INSERT INTO `sp_role`
VALUES (3, '移动开发', '1', 1, 0, 1, '2020-06-19 12:11:59', 3, '2021-08-18 15:55:57', '移动开发角色');
INSERT INTO `sp_role`
VALUES (4, '推送测试者', '1', 1, 0, 2, '2021-03-17 18:00:01', 2, '2021-03-17 18:12:08', NULL);

-- ----------------------------
-- Table structure for sp_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sp_role_menu`;
CREATE TABLE `sp_role_menu`
(
    `id`      bigint(20) NOT NULL AUTO_INCREMENT,
    `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
    `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_role_menu
-- ----------------------------
INSERT INTO `sp_role_menu`
VALUES (19, 4, 2);
INSERT INTO `sp_role_menu`
VALUES (20, 4, 3);
INSERT INTO `sp_role_menu`
VALUES (21, 4, 4);
INSERT INTO `sp_role_menu`
VALUES (25, 1, 2);
INSERT INTO `sp_role_menu`
VALUES (26, 1, 3);
INSERT INTO `sp_role_menu`
VALUES (27, 1, 4);
INSERT INTO `sp_role_menu`
VALUES (28, 1, 6);
INSERT INTO `sp_role_menu`
VALUES (45, 3, 2);
INSERT INTO `sp_role_menu`
VALUES (46, 3, 3);
INSERT INTO `sp_role_menu`
VALUES (47, 3, 4);
INSERT INTO `sp_role_menu`
VALUES (48, 2, 2);
INSERT INTO `sp_role_menu`
VALUES (49, 2, 3);
INSERT INTO `sp_role_menu`
VALUES (50, 2, 4);
INSERT INTO `sp_role_menu`
VALUES (51, 2, 6);

-- ----------------------------
-- Table structure for sp_user
-- ----------------------------
DROP TABLE IF EXISTS `sp_user`;
CREATE TABLE `sp_user`
(
    `user_id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `username`    varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
    `name`        varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
    `password`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
    `email`       varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
    `mobile`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
    `is_admin`    int(11) NOT NULL DEFAULT 0 COMMENT '0--不是admin 1--是admin',
    `sex`         bigint(20) NULL DEFAULT NULL COMMENT '性别',
    `pic_url`     bigint(20) NULL DEFAULT NULL,
    `enable_flag` int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag` int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`user_id`) USING BTREE,
    UNIQUE INDEX `username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_user
-- ----------------------------
INSERT INTO `sp_user`
VALUES (1, 'admin', '管理员', '$2a$10$nbGK7sLJI4wcG8n0gpRTd.7amOft4bAJPUrgAd5roGJTy.fQZSYeO', NULL, NULL, 1, 1, NULL, 1, 0,
        1, '2020-06-19 12:08:59', 1, '2020-08-06 09:53:43', NULL);


-- ----------------------------
-- Table structure for sp_user_app_role
-- ----------------------------
DROP TABLE IF EXISTS `sp_user_app_role`;
CREATE TABLE `sp_user_app_role`
(
    `id`      bigint(20) NOT NULL AUTO_INCREMENT,
    `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
    `app_id`  bigint(20) NULL DEFAULT NULL COMMENT '应用ID',
    `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与应用与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_user_app_role
-- ----------------------------
INSERT INTO `sp_user_app_role`
VALUES (1, 5, 3, 1);
INSERT INTO `sp_user_app_role`
VALUES (4, 7, 4, 4);
INSERT INTO `sp_user_app_role`
VALUES (5, 8, 4, 2);
INSERT INTO `sp_user_app_role`
VALUES (7, 9, 8, 3);
INSERT INTO `sp_user_app_role`
VALUES (8, 10, 8, 1);
INSERT INTO `sp_user_app_role`
VALUES (9, 11, 8, 2);
INSERT INTO `sp_user_app_role`
VALUES (10, 12, 9, 1);
INSERT INTO `sp_user_app_role`
VALUES (11, 13, 9, 3);
INSERT INTO `sp_user_app_role`
VALUES (12, 9, 12, 3);
INSERT INTO `sp_user_app_role`
VALUES (13, 13, 13, 3);
INSERT INTO `sp_user_app_role`
VALUES (14, 14, 14, 3);
INSERT INTO `sp_user_app_role`
VALUES (15, 13, 3, 3);
INSERT INTO `sp_user_app_role`
VALUES (16, 13, 4, 3);
INSERT INTO `sp_user_app_role`
VALUES (17, 15, 13, 3);
INSERT INTO `sp_user_app_role`
VALUES (18, 16, 13, 3);
INSERT INTO `sp_user_app_role`
VALUES (19, 17, 13, 2);
INSERT INTO `sp_user_app_role`
VALUES (20, 16, 15, 3);
INSERT INTO `sp_user_app_role`
VALUES (21, 18, 13, 3);
INSERT INTO `sp_user_app_role`
VALUES (22, 18, 15, 3);

-- ----------------------------
-- Table structure for sp_version
-- ----------------------------
DROP TABLE IF EXISTS `sp_version`;
CREATE TABLE `sp_version`
(
    `id`                          bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `app_id`                      bigint(20) NOT NULL COMMENT '应用ID',
    `platform`                    varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '平台名称',
    `version_name`                varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '版本名称',
    `version_number`              varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '版本号',
    `update_log`                  varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新日志',
    `download_url`                varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下载地址',
    `internal_url`                varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '内部下载url',
    `external_url`                varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '外部下载url',
    `version_config`              varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '版本配置（需要版本更新的版本号例如 10,11,12）',
    `enable_edit`                 int(1) NOT NULL DEFAULT 1 COMMENT '是否可编辑',
    `enable_flag`                 int(1) NOT NULL DEFAULT 1 COMMENT '启用状态',
    `delete_flag`                 int(1) NOT NULL DEFAULT 0 COMMENT '删除状态',
    `create_by`                   bigint(20) NULL DEFAULT NULL COMMENT '创建者',
    `create_date`                 datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`                   bigint(20) NULL DEFAULT NULL COMMENT '更新者',
    `update_date`                 datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `remarks`                     varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
    `need_update_versions`        longtext CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '版本配置（需要版本更新的版本号例如: 1.1.1,1.1.2,1.1.3）',
    `canary_release_stage`        varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '发布阶段（例如 2,10,30,40,50,100,100）',
    `canary_release_enable`       int(1) NOT NULL DEFAULT 0 COMMENT '0--已禁用 1--已启用  是否灰度发布 默认禁用',
    `canary_release_use_time`     bigint(20) NOT NULL DEFAULT 0 COMMENT '灰度发已用时间（毫秒）',
    `old_canary_release_use_time` bigint(20) NOT NULL DEFAULT 0 COMMENT '已用时间（用于启用一段时间关闭的状态保存）',
    `enable_time`                 datetime(0) NULL DEFAULT NULL COMMENT '启用时间',
    `published`                   int(1) NOT NULL DEFAULT 0 COMMENT '发布状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '版本管理' ROW_FORMAT = Dynamic;



SET
FOREIGN_KEY_CHECKS = 1;
