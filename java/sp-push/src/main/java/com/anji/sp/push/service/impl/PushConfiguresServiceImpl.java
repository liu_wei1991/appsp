package com.anji.sp.push.service.impl;

import com.anji.sp.enums.IsDeleteEnum;
import com.anji.sp.enums.RepCodeEnum;
import com.anji.sp.enums.UserStatus;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.mapper.PushConfiguresMapper;
import com.anji.sp.push.model.po.PushConfiguresPO;
import com.anji.sp.push.model.po.PushUserPO;
import com.anji.sp.push.model.vo.PushConfiguresVO;
import com.anji.sp.push.service.PushConfiguresService;
import com.anji.sp.util.BeanUtils;
import com.anji.sp.util.RandCodeUtil;
import com.anji.sp.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 推送配置项 服务实现类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Service
public class PushConfiguresServiceImpl extends ServiceImpl<PushConfiguresMapper, PushConfiguresPO> implements PushConfiguresService {

    @Autowired
    private PushConfiguresMapper pushConfiguresMapper;

    /**
     * 根据数据库必填项，校验是否为空，不校验主键
     *
     * @param pushConfiguresVO
     * @return
     */
    private ResponseModel validateCreateFieldNotNull(PushConfiguresVO pushConfiguresVO) {
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        /* 该片段由生成器产生，请根据实际情况修改
        if(pushConfiguresVO.getId() == null){
            return RepCodeEnum.NULL_ERROR.parseError("id");
        }
        if(pushConfiguresVO.getAppId() == null){
            return RepCodeEnum.NULL_ERROR.parseError("appId");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getSecretKey())){
            return RepCodeEnum.NULL_ERROR.parseError("secretKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getJgAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("jgAppKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getJgMasterSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("jgMasterSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getHwAppId())){
            return RepCodeEnum.NULL_ERROR.parseError("hwAppId");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getHwAppSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("hwAppSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getHwDefaultTitle())){
            return RepCodeEnum.NULL_ERROR.parseError("hwDefaultTitle");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getOpAppId())){
            return RepCodeEnum.NULL_ERROR.parseError("opAppId");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getOpAppSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("opAppSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getOpDefaultTitle())){
            return RepCodeEnum.NULL_ERROR.parseError("opDefaultTitle");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getOpAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("opAppKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getOpMasterSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("opMasterSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getVoAppId())){
            return RepCodeEnum.NULL_ERROR.parseError("voAppId");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getVoAppSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("voAppSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getVoDefaultTitle())){
            return RepCodeEnum.NULL_ERROR.parseError("voDefaultTitle");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getVoAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("voAppKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getXmAppId())){
            return RepCodeEnum.NULL_ERROR.parseError("xmAppId");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getXmAppSecret())){
            return RepCodeEnum.NULL_ERROR.parseError("xmAppSecret");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getXmDefaultTitle())){
            return RepCodeEnum.NULL_ERROR.parseError("xmDefaultTitle");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getXmAppKey())){
            return RepCodeEnum.NULL_ERROR.parseError("xmAppKey");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getPackageName())){
            return RepCodeEnum.NULL_ERROR.parseError("packageName");
        }
        if(pushConfiguresVO.getEnableFlag() == null){
            return RepCodeEnum.NULL_ERROR.parseError("enableFlag");
        }
        if(pushConfiguresVO.getDeleteFlag() == null){
            return RepCodeEnum.NULL_ERROR.parseError("deleteFlag");
        }
        if(pushConfiguresVO.getCreateBy() == null){
            return RepCodeEnum.NULL_ERROR.parseError("createBy");
        }
        if(pushConfiguresVO.getCreateDate() == null){
            return RepCodeEnum.NULL_ERROR.parseError("createDate");
        }
        if(pushConfiguresVO.getUpdateBy() == null){
            return RepCodeEnum.NULL_ERROR.parseError("updateBy");
        }
        if(pushConfiguresVO.getUpdateDate() == null){
            return RepCodeEnum.NULL_ERROR.parseError("updateDate");
        }
        if(StringUtils.isBlank(pushConfiguresVO.getRemarks())){
            return RepCodeEnum.NULL_ERROR.parseError("remarks");
        }
        */
        return ResponseModel.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel create(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        ResponseModel valid = validateCreateFieldNotNull(pushConfiguresVO);
        if (valid.isError()) {
            return valid;
        }
        //业务校验
        //...todo

        //业务处理
        if (pushConfiguresVO.getEnableFlag() == null) {
            pushConfiguresVO.setEnableFlag(UserStatus.OK.getIntegerCode());
        }
        if (pushConfiguresVO.getChannelEnable()==null){
            pushConfiguresVO.setChannelEnable(0);
        }
        pushConfiguresVO.setSecretKey(RandCodeUtil.getUUID());
        pushConfiguresVO.setDeleteFlag(IsDeleteEnum.NOT_DELETE.getIntegerCode());
        pushConfiguresVO.setUpdateBy(1L);
        pushConfiguresVO.setCreateDate(LocalDateTime.now());
        pushConfiguresVO.setUpdateBy(1L);
        pushConfiguresVO.setUpdateDate(LocalDateTime.now());

        PushConfiguresPO pushConfiguresPO = new PushConfiguresPO();
        BeanUtils.copyProperties(pushConfiguresVO, pushConfiguresPO);
        boolean flag = save(pushConfiguresPO);

        //返回结果
        if (flag) {
            return ResponseModel.successData(pushConfiguresPO);
        } else {
            return ResponseModel.errorMsg(RepCodeEnum.ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel updateById(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        Long pushConfiguresId = pushConfiguresVO.getId();
        if (pushConfiguresId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("pushConfiguresId");
        }
        //业务校验
        //...todo

        //业务处理
        PushConfiguresPO pushConfiguresPO = getById(pushConfiguresId);
        if (pushConfiguresPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("pushConfiguresId=" + pushConfiguresId.longValue());
        }
        if (StringUtils.isBlank(pushConfiguresPO.getSecretKey())){
            pushConfiguresVO.setSecretKey(RandCodeUtil.getUUID());
        }
        pushConfiguresVO.setUpdateDate(LocalDateTime.now());
        BeanUtils.copyProperties(pushConfiguresVO, pushConfiguresPO, true);
        boolean flag = updateById(pushConfiguresPO);

        //返回结果
        if (flag) {
            return ResponseModel.successData(pushConfiguresPO);
        } else {
            return ResponseModel.errorMsg("修改失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseModel deleteById(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        Long pushConfiguresId = pushConfiguresVO.getId();
        if (pushConfiguresId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("pushConfiguresId");
        }

        //业务处理
        PushConfiguresPO pushConfiguresPO = getById(pushConfiguresId);
        if (pushConfiguresPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("pushConfiguresId=" + pushConfiguresId.longValue());
        }
        boolean flag = removeById(pushConfiguresId);

        //返回结果
        if (flag) {
            return ResponseModel.successData("删除成功");
        } else {
            return ResponseModel.errorMsg("删除失败");
        }
    }

    @Override
    public ResponseModel queryById(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        Long pushConfiguresId = pushConfiguresVO.getId();
        if (pushConfiguresId == null) {
            return RepCodeEnum.NULL_ERROR.parseError("pushConfiguresId");
        }

        //业务处理
        PushConfiguresPO pushConfiguresPO = getById(pushConfiguresId);
        if (pushConfiguresPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("pushConfiguresId=" + pushConfiguresId.longValue());
        }

        //返回结果
        BeanUtils.copyProperties(pushConfiguresPO, pushConfiguresVO);
        return ResponseModel.successData(pushConfiguresVO);
    }

    @Override
    public ResponseModel select(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        PushConfiguresPO pushConfiguresPO = getOne(pushConfiguresVO);
        if (pushConfiguresPO == null) {
            return ResponseModel.success();
        }
        BeanUtils.copyProperties(pushConfiguresPO, pushConfiguresVO);
        return ResponseModel.successData(pushConfiguresVO);
    }

    @Override
    public ResponseModel selectOne(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }

        QueryWrapper<PushConfiguresPO> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(pushConfiguresVO.getAppKey())){
            queryWrapper.eq("app_key", pushConfiguresVO.getAppKey());
        }
        if (StringUtils.isNotBlank(pushConfiguresVO.getSecretKey())){
            queryWrapper.eq("secret_key", pushConfiguresVO.getSecretKey());
        }
        PushConfiguresPO pushConfiguresPO = getOne(queryWrapper);
        if (pushConfiguresPO == null) {
            return ResponseModel.success();
        }
        BeanUtils.copyProperties(pushConfiguresPO, pushConfiguresVO);
        return ResponseModel.successData(pushConfiguresVO);
    }


    /**
     * 通用查询单条数据
     * @param pushConfiguresVO
     * @return
     */
    private PushConfiguresPO getOne(PushConfiguresVO pushConfiguresVO){
        //业务处理
        QueryWrapper<PushConfiguresPO> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(pushConfiguresVO.getAppKey())){
            queryWrapper.eq("app_key", pushConfiguresVO.getAppKey());
        } else if (Objects.nonNull(pushConfiguresVO.getAppId())){
            queryWrapper.eq("app_id", pushConfiguresVO.getAppId());
        }
        List<PushConfiguresPO> pushConfiguresPOS = pushConfiguresMapper.selectList(queryWrapper);
        if (pushConfiguresPOS.size()>0){
            return pushConfiguresPOS.get(0);
        }
        return null;
    }

    @Override
    public ResponseModel queryByAppKey(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        String appKey = pushConfiguresVO.getAppKey();
        if (appKey == null) {
            return RepCodeEnum.NULL_ERROR.parseError("appKey");
        }

        //业务处理
        QueryWrapper<PushConfiguresPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("app_key", appKey);
        PushConfiguresPO pushConfiguresPO = getOne(queryWrapper);

        if (pushConfiguresPO == null) {
            return RepCodeEnum.NOT_EXIST_ERROR.parseError("数据");
        }
        //返回结果
        return ResponseModel.successData(pushConfiguresPO);
    }

    @Override
    public ResponseModel queryByPage(PushConfiguresVO pushConfiguresVO) {
        //参数校验
        if (pushConfiguresVO == null) {
            return RepCodeEnum.NULL_ERROR.parseError("requestModel data");
        }
        //...todo

        //分页参数
        Page<PushConfiguresVO> page = new Page<PushConfiguresVO>(pushConfiguresVO.getPageNo(), pushConfiguresVO.getPageSize());
        pushConfiguresVO.setDeleteFlag(IsDeleteEnum.NOT_DELETE.getIntegerCode());

        //业务处理
        IPage<PushConfiguresVO> pageList = pushConfiguresMapper.queryByPage(page, pushConfiguresVO);

        //返回结果
        return ResponseModel.successData(pageList);
    }
}
