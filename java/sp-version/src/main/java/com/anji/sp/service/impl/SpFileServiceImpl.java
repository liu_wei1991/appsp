package com.anji.sp.service.impl;

import com.anji.sp.mapper.SpFileMapper;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.model.po.SpFilePO;
import com.anji.sp.service.SpFileService;
import com.anji.sp.util.SecurityUtils;
import com.anji.sp.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

/**
 * @author : kean_qi
 * create at:  2020/6/26  11:36 上午
 * @description:
 */
@Service
@Slf4j
public class SpFileServiceImpl implements SpFileService {
    @Autowired
    SpFileMapper spFileMapper;
    @Override
    public ResponseModel select(SpFilePO spFilePO) {
        if (StringUtils.isEmpty(spFilePO.getFileId())){
            return ResponseModel.errorMsg("文件id不存在");
        }
        QueryWrapper<SpFilePO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("file_id", spFilePO.getFileId());
        SpFilePO po = spFileMapper.selectOne(queryWrapper);
        if (Objects.nonNull(po)){
            return ResponseModel.successData(po);
        }
        return ResponseModel.errorMsg("文件不存在");
    }


    @Override
    public ResponseModel insert(SpFilePO spFilePO) {
        if (StringUtils.isEmpty(spFilePO.getFileId())){
            return ResponseModel.errorMsg("文件id不存在");
        }
        if (StringUtils.isEmpty(spFilePO.getFilePath())){
            return ResponseModel.errorMsg("文件完整目录不存在");
        }
        if (StringUtils.isEmpty(spFilePO.getUrlPath())){
            return ResponseModel.errorMsg("文件请求URL不存在");
        }
        spFilePO.setCreateBy(SecurityUtils.getUserId());
        spFilePO.setUpdateBy(SecurityUtils.getUserId());
        spFilePO.setCreateDate(new Date());
        spFilePO.setUpdateDate(new Date());
        int insert = spFileMapper.insert(spFilePO);
        if (insert>0){
            return ResponseModel.successData(spFilePO);
        }
        return ResponseModel.errorMsg("插入失败");
    }
}
