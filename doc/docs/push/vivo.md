# 推送-vivo  

## 官网配置  

1、点击首页中的“消息推送”，如下图所示：  
![avatar](../assets/vivo1.png)  
2、点击“推送申请” ，如下图所示 
![avatar](../assets/vivo2.png)
3、若应用已经创建，则选择应用，并提交申请，如下图所示 
![avatar](../assets/vivo3.png)  

## 推送测试  

1、应用未发布，则推送权限受限，可进行API接口测试 

3、应用已发布

3.1，新建测试设备
![avatar](../assets/push_vivo_test_devices.png)  

3.2，在“推送工具”-“”，点击“新建推送”，填写推送信息，点击“确认推送”，如下图所示：
![avatar](../assets/vivo6.png)  
![avatar](../assets/vivo7.png)