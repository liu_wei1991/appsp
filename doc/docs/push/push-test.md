# 操作手册  

1，移动服务平台（Appsp）创建应用
![avatar](../assets/step_app.png)  

2，进入应用后，选择“推送管理”-“推送配置”,将极光和厂商信息完善（若需使用厂商通道），具体可参考各厂商推送文档
![avatar](../assets/push_config.png)  

3，App集成参考PushDemo，为了测试是否配置成功，需要进行推送测试，可以在极光官网或者各大厂商平台测试，我们推荐在移动服务平台测试

3.1  消息推送
![avatar](../assets/push_msg.png) 

3.2  消息透传
![avatar](../assets/payload.png) 

4，推送历史查看
![avatar](../assets/push_history.png) 

